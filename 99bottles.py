#!/usr/bin/env python2
# -*- coding: utf-8 -*-

for i in range(99,0,-1):
    if i != 1:
        print i, 'bottles of beer on the wall,'
        print i, 'bottles of beer.'
    else:
        print i, 'bottle of beer on the wall,'
        print i, 'bottle of beer.'
    print 'Take one down, pass it around,'
    if i > 2:
        print i-1, 'bottles of beer on the wall.'
    elif i == 2:
        print '1 bottle of beer on the wall.'
    else:
        print 'No more bottles of beer on the wall.'
    print

print """No more bottles of beer on the wall,
no more bottles of beer.
Go to the store and buy some more,
99 bottles of beer on the wall."""
